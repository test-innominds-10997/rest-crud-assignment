package com.spring.rest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	
	private Long empid;
	
	private String fname;
	
	private String lname;
	
	private String email;
	
	private Long phnno;
	
	private String hiredate;
	
	private int jobid;
	
	private Long salary;
	
	private Long managerid;

	

	public Long getEmpid() {
		return empid;
	}

	public void setEmpid(Long empid) {
		this.empid = empid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhnno() {
		return phnno;
	}

	public void setPhnno(Long phnno) {
		this.phnno = phnno;
	}

	public String getHiredate() {
		return hiredate;
	}

	public void setHiredate(String hiredate) {
		this.hiredate = hiredate;
	}

	public int getJobid() {
		return jobid;
	}

	public void setJobid(int jobid) {
		this.jobid = jobid;
	}

	public Long getSalary() {
		return salary;
	}

	public void setSalary(Long salary) {
		this.salary = salary;
	}

	public Long getManagerid() {
		return managerid;
	}

	public void setManagerid(Long managerid) {
		this.managerid = managerid;
	}

	public Employee(Long empid,String fname,String lname,String email, Long phnno, String hiredate,int jobid, Long salary, Long managerid) {
		super();
		this.empid = empid;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.phnno = phnno;
		this.hiredate = hiredate;
		this.jobid = jobid;
		this.salary = salary;
		this.managerid = managerid;

	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
