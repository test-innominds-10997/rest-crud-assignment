package com.spring.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.rest.entity.Employee;
import com.spring.rest.entity.Student;
import com.spring.rest.entity.User;
import com.spring.rest.repository.EmployeeRepository;
import com.spring.rest.repository.StudentRepository;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeRepository employeeRepo;
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployee() {
		return employeeRepo.findAll();
	}
	
	

	@GetMapping("/employees/{id}")
	public Employee getOneEmployees(@PathVariable("id") Long id) {
		return employeeRepo.getById(id);
		
		
	}
	
	@PostMapping("/employees")
	public Employee addEmployees(@RequestBody Employee requestEmployees) {
		
		return employeeRepo.save(requestEmployees);
	}
	
	
	@PutMapping("/employees/{id}")
	public Employee updateEmployees(@PathVariable("id") Long id, @RequestBody Employee newEmployeesValue)
	{
		
		Employee e = employeeRepo.getById(id);
		
		if(e!=null)
		{
			e.setEmpid(newEmployeesValue.getEmpid());
			e.setFname(newEmployeesValue.getFname());
			e.setLname(newEmployeesValue.getLname());
			e.setEmail(newEmployeesValue.getEmail());
			e.setPhnno(newEmployeesValue.getPhnno());
			e.setHiredate(newEmployeesValue.getHiredate());
			e.setJobid(newEmployeesValue.getJobid());
			e.setSalary(newEmployeesValue.getSalary());
			e.setManagerid(newEmployeesValue.getManagerid());
		}
		return employeeRepo.save(e);
	
	
}
	
	@DeleteMapping("/employees/{id}")
	
	public String deleteEmployees(@PathVariable("id") Long id)
	{
		Employee e = employeeRepo.getById(id);
		
		if(e != null)
		{
			employeeRepo.delete(e);
			return "employee deleted";
		}
		else
		{
			return "employee not available";
		}
	
		
		
}
	
	
}